const PerformanceCalculator = require('../../../src/calculator/PerformanceCalculator')

describe('PerformanceCalculator tests', () => {
  it('Throw error', () => {
    const errPlay = {
      name: 'Alberto Eyo play',
      type: 'Alberto_Eyo_Type'
    }
    const performance = {
      playID: 'eyo',
      audience: 80
    }
    const normalCalculator = new PerformanceCalculator(performance, errPlay)

    expect(() => normalCalculator.amount).toThrow()
  })
})