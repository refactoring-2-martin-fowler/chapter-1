const {statement, htmlStatement} = require('../index')

describe('Testing the statement functions', () => {
  const plays = {
    'hamlet': {
      'name': 'Hamlet',
      'type': 'tragedy'
    },
    'as-like': {
      'name': 'As You Like It',
      'type': 'comedy'
    },
    'othello': {
      'name': 'Othello',
      'type': 'tragedy'
    }
  }
  const invoices = [
    {
      'customer': 'BigCo',
      'performances': [
        {
          'playID': 'hamlet',
          'audience': 55
        },
        {
          'playID': 'as-like',
          'audience': 35
        },
        {
          'playID': 'othello',
          'audience': 40
        }
      ]
    }
  ]
  it('Should return the output for the BigCo customer', () => {
    const out = statement(invoices[0], plays)
    const output = `Statement for BigCo
  Hamlet: $650.00 (55 seats)
  As You Like It: $580.00 (35 seats)
  Othello: $500.00 (40 seats)
Amount owed is $1,730.00
You earned 47 credits
`
    expect(out).toEqual(output)
  })
  it('Should return the html info for the BigCo customer', () => {
    const out = htmlStatement(invoices[0], plays)
    const output = `<h1>Statement for BigCo</h1>
<table>
<tr><th>play</th><th>seats</th><th>cost</th></tr>  <tr><td>Hamlet</td><td>55</td><td>$650.00</td></tr>
  <tr><td>As You Like It</td><td>35</td><td>$580.00</td></tr>
  <tr><td>Othello</td><td>40</td><td>$500.00</td></tr>
</table>
<p>Amount owed is <em>$1,730.00</em></p>
<p>You earned <em>47</em> credits</p>
`
    expect(out).toEqual(output)
  })
  it('Should raise an exception when the play type is not recognized', () => {
    const errPlays = {
      eyo: {
        name: 'Alberto Eyo play',
        type: 'Alberto_Eyo_Type'
      }
    }
    const invoicesError = [{
      customer: 'Eyo',
      performances: [{
        playID: 'eyo',
        audience: 80
      }]
    }]
    expect(() => statement(invoicesError[0], errPlays)).toThrow()
  })
})